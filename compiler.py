#!/usr/bin/env python
import sys
import collections
import inspect, os.path
import re

DEBUG = True

def lineno():
    """Returns the current line number in program"""
    cur_frame = inspect.currentframe()
    outer_frame = inspect.getouterframes(cur_frame)[2]
    return '%s:%s' % (os.path.basename(outer_frame[1]), outer_frame[2])

def debug(msg):
  if DEBUG:
    print '[DEBUG]', '[%s]' % lineno(), msg

def error(msg):
  print '[ERROR]', '[%s]' % lineno(), msg

# TODO add line/position
def tokenize(source_code):
  """
    returns
    '('
    ')'
    '['
    ']'
    'literal'
    'digit'
  """
  literal = ''
  digits = ''
  for c in source_code:
    if literal and not (c.isalpha() or c.isdigit()):
      yield literal
      literal = ''
    if digits and not c.isdigit():
      yield int(digits)
      digits = ''

    if c == '(':
      yield '('
    elif c == ')':
      yield ')'
    elif c == '[':
      yield '['
    elif c == ']':
      yield ']'
    elif c.isalpha():
      literal += c
    elif c.isdigit():
      if literal:
        literal += c
      else:
        digits += c


def build_raw_ast(tokens):
  depth = 0
  token_ptr = 0
  state = 'START'
  stack = []
  depth = 0
  for token in tokens:
    if token == '(':
      depth += 1
      stack.append([])
    elif token == ')':
      depth -= 1
      if depth < 0:
        error('Unbalanced closed parantheses')
        exit(1)
      if len(stack) > 1:
        child = stack.pop()
        stack[-1].append(child)
    else:
      stack[-1].append(token)

  return stack


class Digit:
  def __init__(self, value):
    self.value = value
    self.__repr__ = self.__str__
  def __str__(self):
    return str(self.value)

class Literal:
  def __init__(self, name):
    self.name = name
    self.__repr__ = self.__str__
  def __str__(self):
    return str(self.name)

class Def:
  def __init__(self, name, expr):
    self.name = name
    self.expr = expr
    self.__repr__ = self.__str__
  def __str__(self):
    # return "(def %s %s)" % str(self.name), str(self.expr)
    return '(def ' + str(self.name) + ' ' + str(self.expr) + ')'

class Deffn:
  def __init__(self, name, args, expression):
    self.name = name
    self.args = args
    self.expression = expression
    self.__repr__ = self.__str__
  def __str__(self):
    return "(defn %s %s %s)" % (self.name, self.args, self.expression)

class Sequence:
  def __init__(self, expressions):
    self.expressions = expressions
    self.__repr__ = self.__str__
  def __str__(self):
    return "seq%s" % self.expressions

class Eval:
  def __init__(self, literal, expressions):
    self.literal = literal
    self.expressions = expressions
    self.__repr__ = self.__str__
  def __str__(self):
    # return "eval{%s %s}" % str(self.literal), str(self.expressions)
    return 'eval{' + str(self.literal) + ' ' + str(self.expressions) + '}'

class GenContext:
  def __init__(self, parent_ctx = None):
    self.main_space = []
    self.functions_space = []
    self.names = {}
    self.fun_names = {}
    self.fun_args = []
    self.parent_ctx = parent_ctx
    self.stack_level = 0
    if parent_ctx:
      self.stack_level = parent_ctx.stack_level + 1
    self.__repr__ = self.__str__

  def result_code(self):
    result = []
    if self.functions_space:
      main_ptr = 3 + len(self.functions_space)
      result.append(("LDF %d", main_ptr, '-call <main>'))
      result.append(("AP 0", None, ''))
      result.append(('RTN', None, 'end <main>'))
      result.extend(self.functions_space)
    result.extend(self.main_space)
    return result

  def append_to_fspace(self, code):
    self.functions_space.append(code)

  def fspace_ptr(self):
    return len(self.functions_space) + 3

  def mspace_ptr(self):
    return len(self.main_space) + 3

  def append_to_mspace(self, code):
    self.main_space.append(code)

  def __str__(self):
    if self.parent_ctx:
      return 'ctx [lvl%s] names: %s, fun_names: %s, fun_args: %s\n\t%s-> parent_ctx: %s' % (self.stack_level, self.names, self.fun_names, self.fun_args, self.stack_level-1, self.parent_ctx)
    else:
      return 'ctx [lvl%s] names: %s, fun_names: %s, fun_args: %s' % (self.stack_level, self.names, self.fun_names, self.fun_args)

predefined = {}
predefined['add'] = 2
predefined['sub'] = 2
predefined['mul'] = 2
predefined['div'] = 2
predefined['ceq'] = 2
predefined['cgt'] = 2
predefined['cgte'] = 2
predefined['cons'] = 2
predefined['car'] = 1
predefined['atom'] = 1
predefined['cdr'] = 2


def translate(raw_ast):
  literal_re = re.compile("^(\w[0-9]*)+$", )
  if isinstance(raw_ast, collections.MutableSequence):
    if isinstance(raw_ast[0], collections.MutableSequence):
      return Sequence(map(translate, raw_ast))
    elif raw_ast[0] == 'def':
      return Def(raw_ast[1], translate(raw_ast[2]))
    elif raw_ast[0] == 'defn':
      return Deffn(raw_ast[1], raw_ast[2:-1], translate(raw_ast[-1]))
    elif literal_re.match(raw_ast[0]):
      return Eval(raw_ast[0], [translate(node) for node in raw_ast[1:]])
    else:
      error("Could not translate to ast node %s" % raw_ast[0])
      exit(1)
  elif isinstance(raw_ast, int):
    return Digit(raw_ast)
  else:
    return Literal(raw_ast)


def generate(ast, ctx):
  debug('gen AST node %s' % ast)
  if isinstance(ast, Digit):
    # put digit to Data Stack
    ctx.append_to_mspace(("LDC %d" % ast.value, None, ''))
  elif isinstance(ast, Literal):
    if ast.name in ctx.names and ast.name in ctx.fun_args:
      error("Name clash %s" % ast.name)
      exit(1)
    if ast.name in ctx.names:
      code_ptr = ctx.names[ast.name]
      # create closure to def in Data Stack
      ctx.append_to_mspace(('LDF %d', code_ptr, '-load var %s' % ast.name))
      # go to def start, create empty Env frame
      ctx.append_to_mspace(('AP 0', None, ''))
    elif ast.name in ctx.fun_args:
      arg_ptr = ctx.fun_args.index(ast.name)
      # load argument from last Env frame (by arg index)
      ctx.append_to_mspace(("LD 0 %d" % arg_ptr, None, '-load arg %s' % ast.name))
    else:
      error("Cannot resolve literal %s" % ast.name)
      exit(1)
  elif isinstance(ast, Def):
    current_ptr = ctx.fspace_ptr()
    # save pointer to def start by name
    ctx.names[ast.name] = current_ptr
    # create child context for def body
    expr_ctx = GenContext(ctx)
    # move back all def start pointers in child context
    for (n, p) in ctx.names.iteritems():
      expr_ctx.names[n] = p - current_ptr
      debug("move back (%s, %s) -> [%s] = %s" % (n, p, n, p - current_ptr))
    # move back all defn start pointers in child context
    for (n, (p, num_args)) in ctx.fun_names.iteritems():
      expr_ctx.fun_names[n] = (p - current_ptr, num_args)
      debug("move back (%s, %s) -> [%s] = %s" % (n, p, n, p - current_ptr))
    # propagate defn arguments to child context
    expr_ctx.fun_args = ctx.fun_args
    generate(ast.expr, expr_ctx)
    first = True
    fspace_start = 0
    for (code, ptr, comment) in expr_ctx.result_code():
      # shifting offset
      if not ptr is None:
        ptr += current_ptr
      if first:
        fspace_start = ptr or '_'
        comment = 'def %s %s' % (ast.name, comment)
        first = False
      ctx.append_to_fspace((code, ptr, comment))
    ctx.append_to_fspace(('RTN', None, 'end:%s def %s' % (fspace_start, ast.name)))
  elif isinstance(ast, Deffn):
    current_ptr = ctx.fspace_ptr()
    ctx.fun_names[ast.name] = (current_ptr, len(ast.args))
    expr_ctx = GenContext(ctx)
    # move back all def start pointers in child context
    for (n, p) in ctx.names.iteritems():
      debug("move back (%s, %s) -> [%s] = %s" % (n, p, n, p - current_ptr))
      expr_ctx.names[n] = p - current_ptr
    # move back all defn start pointers in child context
    for (n, (p, num_args)) in ctx.fun_names.iteritems():
      debug("move back (%s, %s) -> [%s] = %s" % (n, p, n, p - current_ptr))
      expr_ctx.fun_names[n] = (p - current_ptr, num_args)
    # no nested functions
    expr_ctx.fun_args = ast.args
    generate(ast.expression, expr_ctx)
    first = True
    fspace_start = 0
    for (code, ptr, comment) in expr_ctx.result_code():
      # shifting offset
      if not ptr is None:
        ptr += current_ptr
      if first:
        fspace_start = ptr or '_'
        comment = 'defn %s' %  ', '.join([ast.name, comment])
        first = False
      ctx.append_to_fspace((code, ptr, comment))
    ctx.append_to_fspace(('RTN', None, 'end:%s defn %s' % (fspace_start, ast.name)))
  elif isinstance(ast, Sequence):
    for expr in ast.expressions:
      generate(expr, ctx)
  elif isinstance(ast, Eval):
    arg_num = 0
    eval_commands = []
    if ast.literal in predefined:
      arg_num = predefined[ast.literal]
      eval_commands.append((ast.literal.upper(), None, ''))
    elif ast.literal in ctx.fun_names:
      (f_ptr, arg_num) = ctx.fun_names[ast.literal]
      eval_commands.append(('LDF %d', f_ptr, '-call %s' % ast.literal))
      eval_commands.append(('AP %d' % arg_num, None, ''))
    else:
      error('Unknown function %s' % ast.literal)
      exit(1)
    if len(ast.expressions) != arg_num:
      error('Expected %d arguments for fucntion %s' % (arg_num, ast.literal))
      exit(1)
    current_ptr = ctx.mspace_ptr() # TODO !!!!! should shift this ptr after each iteration?
    for e in ast.expressions:
      expr_ctx = GenContext(ctx)
      for (n, p) in ctx.names.iteritems():
        expr_ctx.names[n] = p - current_ptr
      for (n, (p, num_args)) in ctx.fun_names.iteritems():
        expr_ctx.fun_names[n] = (p - current_ptr, num_args)
      expr_ctx.fun_args = ctx.fun_args
      generate(e, expr_ctx)
      for (code, ptr, comment) in expr_ctx.result_code():
        # shifting offset
        if not ptr is None:
          ptr += current_ptr
        ctx.append_to_mspace((code, ptr, comment))
    for (code, ptr, comment) in eval_commands:
      ctx.append_to_mspace((code, ptr, comment))
  else:
    error('Unknown AST node %s' % ast)
    exit(1)
  debug('generated context for node %s: \n%s' % (ast, ctx))


if len(sys.argv) < 2:
  default_file = 'fn3.lisp'
  print "You didn't specify lisp file, %s will be used" % default_file
  sys.argv.append(default_file)

lisp_code = ' '.join(list(open(sys.argv[1], 'r')))
debug('%s file contents: \n%s' % (sys.argv[1], lisp_code))

tokens = list(tokenize(lisp_code))

debug('tokenized: \n%s' % tokens)
raw_ast = build_raw_ast(tokens)
debug('built raw ast: \n%s' % raw_ast)

ast = translate(raw_ast[0])
debug('build AST: \n%s' % ast)

ctx = GenContext()
generate(ast, ctx)

print '============================================'
line_no = 0
for (code, ptr, comment) in ctx.result_code():
  if not ptr is None:
    print code % ptr, '\t;', line_no, comment
  else:
    print code, '\t;', line_no, comment
  line_no += 1
print 'STOP'
