#!/usr/bin/env python
import sys
import collections
import inspect, os.path
import re

DEBUG = True

def lineno():
    """Returns the current line number in program"""
    cur_frame = inspect.currentframe()
    outer_frame = inspect.getouterframes(cur_frame)[2]
    return '%s:%s' % (os.path.basename(outer_frame[1]), outer_frame[2])

def debug(msg):
  if DEBUG:
    print '[DEBUG]', '[%s]' % lineno(), msg

def error(msg):
  print '[ERROR]', '[%s]' % lineno(), msg

# TODO add line/position
def tokenize(source_code):
  """
    returns
    '('
    ')'
    '['
    ']'
    'literal'
    'digit'
  """
  literal = ''
  digits = ''
  for c in source_code:
    if literal and not (c.isalpha() or c.isdigit()):
      yield literal
      literal = ''
    if digits and not c.isdigit():
      yield int(digits)
      digits = ''

    if c == '(':
      yield '('
    elif c == ')':
      yield ')'
    elif c == '[':
      yield '['
    elif c == ']':
      yield ']'
    elif c.isalpha():
      literal += c
    elif c.isdigit():
      if literal:
        literal += c
      else:
        digits += c


def build_raw_ast(tokens):
  depth = 0
  token_ptr = 0
  state = 'START'
  stack = []
  depth = 0
  for token in tokens:
    if token == '(':
      depth += 1
      stack.append([])
    elif token == ')':
      depth -= 1
      if depth < 0:
        error('Unbalanced closed parantheses')
        exit(1)
      if len(stack) > 1:
        child = stack.pop()
        stack[-1].append(child)
    else:
      stack[-1].append(token)

  return stack


class Digit:
  def __init__(self, value):
    self.value = value
    self.__repr__ = self.__str__
  def __str__(self):
    return str(self.value)

class Literal:
  def __init__(self, name):
    self.name = name
    self.__repr__ = self.__str__
  def __str__(self):
    return str(self.name)

class Def:
  def __init__(self, name, expr):
    self.name = name
    self.expr = expr
    self.__repr__ = self.__str__
  def __str__(self):
    # return "(def %s %s)" % str(self.name), str(self.expr)
    return '(def ' + str(self.name) + ' ' + str(self.expr) + ')'

class Deffn:
  def __init__(self, name, args, expression):
    self.name = name
    self.args = args
    self.expression = expression
    self.__repr__ = self.__str__
  def __str__(self):
    return "(defn %s %s %s)" % (self.name, self.args, self.expression)

class Sequence:
  def __init__(self, expressions):
    self.expressions = expressions
    self.__repr__ = self.__str__
  def __str__(self):
    return "seq%s" % self.expressions
  def def_names(self):
    return map(lambda val: val.name, filter(lambda expr: isinstance(expr, Def), self.expressions))

class Eval:
  def __init__(self, literal, expressions):
    self.literal = literal
    self.expressions = expressions
    self.__repr__ = self.__str__
  def __str__(self):
    # return "eval{%s %s}" % str(self.literal), str(self.expressions)
    return 'eval{' + str(self.literal) + ' ' + str(self.expressions) + '}'

class GenContext_:
  def __init__(self, parent_ctx = None):
    self.main_space = []
    self.functions_space = []
    self.names = {}
    self.fun_names = {}
    self.fun_args = []
    self.parent_ctx = parent_ctx
    self.stack_level = 0
    if parent_ctx:
      self.stack_level = parent_ctx.stack_level + 1
    self.__repr__ = self.__str__

  def result_code(self):
    result = []
    if self.functions_space:
      main_ptr = 3 + len(self.functions_space)
      result.append(("LDF %d", main_ptr, '-call <main>'))
      result.append(("AP 0", None, ''))
      result.append(('RTN', None, 'end <main>'))
      result.extend(self.functions_space)
    result.extend(self.main_space)
    return result

  def append_to_fspace(self, code):
    self.functions_space.append(code)

  def fspace_ptr(self):
    return len(self.functions_space) + 3

  def mspace_ptr(self):
    return len(self.main_space) + 3

  def append_to_mspace(self, code):
    self.main_space.append(code)

  def __str__(self):
    if self.parent_ctx:
      return 'ctx [lvl%s] names: %s, fun_names: %s, fun_args: %s\n\t%s-> parent_ctx: %s' % (self.stack_level, self.names, self.fun_names, self.fun_args, self.stack_level-1, self.parent_ctx)
    else:
      return 'ctx [lvl%s] names: %s, fun_names: %s, fun_args: %s' % (self.stack_level, self.names, self.fun_names, self.fun_args)

predefined = {}
predefined['add'] = 2
predefined['sub'] = 2
predefined['mul'] = 2
predefined['div'] = 2
predefined['ceq'] = 2
predefined['cgt'] = 2
predefined['cgte'] = 2
predefined['cons'] = 2
predefined['car'] = 1
predefined['atom'] = 1
predefined['cdr'] = 2


def translate(raw_ast):
  literal_re = re.compile("^(\w[0-9]*)+$", )
  if isinstance(raw_ast, collections.MutableSequence):
    if isinstance(raw_ast[0], collections.MutableSequence):
      return Sequence(map(translate, raw_ast))
    elif raw_ast[0] == 'def':
      return Def(raw_ast[1], translate(raw_ast[2]))
    elif raw_ast[0] == 'defn':
      return Deffn(raw_ast[1], raw_ast[2:-1], translate(raw_ast[-1]))
    elif literal_re.match(raw_ast[0]):
      return Eval(raw_ast[0], [translate(node) for node in raw_ast[1:]])
    else:
      error("Could not translate to ast node %s" % raw_ast[0])
      exit(1)
  elif isinstance(raw_ast, int):
    return Digit(raw_ast)
  else:
    return Literal(raw_ast)

main_offset = 3

class FunctionsCtx:
  def __init__(self):
    self.fcodes = []
    self.foffset = main_offset

  def add_function_code(self, code, name):
    result = self.foffset
    (c, p, comment) = code[0]
    comment = 'fun:%s %s' % (name, comment)
    code[0] = (c, p, comment) # patch comment
    self.fcodes.append(code)
    self.foffset += len(code)
    code.append(('RTN', None, 'end fun:%s' % name))
    self.foffset += 1
    return result

class GenContext():
  def __init__(self, parent = None):
    self.code = []
    self.names = []
    self.funcs = {}
    self.parent = parent

  def lookup_val(self, name):
    if name in self.names:
      return (0, self.names.index(name))
    elif self.parent:
      pair = self.parent.lookup_val(name)
      if pair:
        (fp, vp) = pair
        return (fp + 1, vp)
      return None
    else:
      error('val "%s" not found' % name)
      exit(1)

  def lookup_fun(self, name):
    if name in self.funcs:
      return self.funcs[name]
    elif self.parent:
      return self.parent.lookup_fun(name)
    else:
      error('val "%s" not found', name)
      exit(1)


functions = FunctionsCtx()

def generate(ast, ctx):
  if isinstance(ast, Digit):
    ctx.code.append(('LDC %d' % ast.value, None, ''))
  elif isinstance(ast, Literal):
    pair = ctx.lookup_val(ast.name)
    if pair:
      ctx.code.append(('LD %d %d' % pair, None, 'load val %s' % ast.name))
  elif isinstance(ast, Def):
    pair = ctx.lookup_val(ast.name)
    if pair:
      (fp, env_p) = pair
      generate(ast.expr, ctx)
      ctx.code.append(('ST %d %d' % (fp, env_p), None, 'set val %s' % ast.name))
    else:
      error('Bug in our code!!')
      exit(1)
  elif isinstance(ast, Deffn):
    function_ctx = GenContext(ctx)
    function_ctx.names = ast.args
    generate(ast.expression, function_ctx)
    foffset = functions.add_function_code(function_ctx.code, ast.name)
    ctx.funcs[ast.name] = (foffset, len(ast.args))
  elif isinstance(ast, Sequence):
    local_names = ast.def_names()
    function_ctx = GenContext(ctx)
    function_ctx.names = local_names
    for expr in ast.expressions:
      generate(expr, function_ctx)
    foffset = functions.add_function_code(function_ctx.code, 'seq')
    for idx, name in enumerate(ast.def_names()):
      ctx.code.append(('LDC 0', None, 'alloc val %s' % name))
    ctx.code.append(('LDF %d', foffset, 'exec sequence'))
    ctx.code.append(('AP %d' %len(local_names), None, ''))
  elif isinstance(ast, Eval):
    arg_num = 0
    eval_commands = []
    if ast.literal in predefined:
      arg_num = predefined[ast.literal]
      eval_commands.append((ast.literal.upper(), None, ''))
    else:
      (f_ptr, arg_num) = ctx.lookup_fun(ast.literal)
      if len(ast.expressions) != arg_num:
        error('Expected %d arguments for fucntion %s' % (arg_num, ast.literal))
        exit(1)
      eval_commands.append(('LDF %d', f_ptr, '-call %s' % ast.literal))
      eval_commands.append(('AP %d' % arg_num, None, ''))

    if len(ast.expressions) != arg_num:
      error('Expected %d arguments for fucntion %s' % (arg_num, ast.literal))
      exit(1)
    for expr in ast.expressions:
      generate(expr, ctx)
    for ec in eval_commands:
      ctx.code.append(ec)
  else:
    print 'Not found %s' % ast

def expand(ctx):
  all_functions = []
  for fcode in functions.fcodes:
    all_functions.extend(fcode)
  result = []
  result.append(('LDF %d' % (main_offset + len(all_functions)), None, 'main_ptr'))
  result.append(('AP 0', None, 'run main'))
  result.append(('RTN', None, 'exit program'))
  result.extend(all_functions)
  result.extend(ctx.code)
  result.append(('RTN', None, 'exit main'))
  return result

if len(sys.argv) < 2:
  default_file = 'var3.lisp'
  print "You didn't specify lisp file, %s will be used" % default_file
  sys.argv.append(default_file)

lisp_code = ' '.join(list(open(sys.argv[1], 'r')))
debug('%s file contents: \n%s' % (sys.argv[1], lisp_code))

tokens = list(tokenize(lisp_code))

debug('tokenized: \n%s' % tokens)
raw_ast = build_raw_ast(tokens)
debug('built raw ast: \n%s' % raw_ast)

ast = translate(raw_ast[0])
debug('build AST: \n%s' % ast)

ctx = GenContext()
generate(ast, ctx)

print '============================================'
line_no = 0

for (code, ptr, comment) in expand(ctx):
  if not ptr is None:
    print code % ptr, '\t;', line_no, comment
  else:
    print code, '\t;', line_no, comment
  line_no += 1
